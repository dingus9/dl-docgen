# PiperCI Sphinx Docs

## Bootstrapping a new project

The simplest option is to clone this repo and set config options in your make.conf to point to a DOCS_YML or RELEASE_YML. See examples provided for an example conf.py. The `source` directory should be used for a static conf.py and index.rst however a component may be defined to build the site_root "`site_root: yes`". This is recommended as it fully separates site content from site generation and CI/CD pipelines. Think, templates(jinja, css etc.) separate from content.

`/source` should be used for customizing template, css and other dynamic generation code as `source` is copied as the base for the `build` directory.

## Makefile

### Make configuration

`make.conf` is used to configure your docs site.

Important config values:

```
DESTDIR       = artifact  # Destination directory for the html site
BUILDDIR      = build     # Final build staging area for conf.py, css, and compiled rst files. Sphinx looks here to build your final site.
CACHEDIR      = cache     # The sphinx cache dir and pre-build staging cache directory for raw component git repositories.
SOURCEDIR     = source    # Your projects templates and source static content... you can use this for conf.py, and site root rst files if you choose, or use the dynamic option as well.
RELEASE       = altstadt  # The release name for your site, required by RELEASE_YML

## Build site from DOCS_YML or RELEASE_YML (mutually exclusive)
DOCS_YML      =           # Use a docs yaml file directly from this URI, supports http/s and local paths
RELEASE_YML   =           # Use a release yaml file + RELEASE to load the docs yaml file.

```

`make stage` downloads all the dependency sites defined in the `piperci-releases/release.yaml` or `docs.yml`
`make requirements` installs requirements in the root requirements.txt and ensures base dependencies are installed prior to running stage
`make component-requirements` installs requirements defined in the docs.yml required to build the html with sphinx
`make html` builds the html site and saves it to artifact for deployment
`make markdown` builds a markdown version of the site
`make requirements` installs base requirements for the build process

### Building

```
make requirements
make stage
make componenet-requirements
make html
```

### Cleaning

#### Clean all the things

```
make clean-all
```

#### Clean the artifact (html site)

```
make clean
```

#### Clean just sphinx caches (not downloaded content in /build)

```
make clean-cache
```

#### Clean the downloaded content in build (used to build the actual site)

```
make clean-build
```

## Building a single site from multiple sources

Docs site generation can be built from local(this) and remote(component) repositories. You may use a hybrid of static docs pages defined here and dynamically included files from other repositories. The end site is built using sphinx, but the site is defined in a docs.yml file located here or remotely.

### Docs.yml Syntax

### README.md auto-include

 To include a repositories README.md without getting fancy only add an entry for the repository and the README.md/rst will be auto added

 More specifically the [docs.yml](https://gitlab.com/dreamer-labs/piperci/piperci-releases/blob/master/altstadt/docs.yml) is used to configure your site.

### More granular files specifications

In addition to README auto-inclusion, additional files and options may be specified for each component.

```yaml
docs:
...
  - name: site_root
    site_root: true
    uri: "https://gitlab.com/dreamer-labs/piperci/piperci-docs.git"
    version: "master"
    files:
      - index.rst
      - piperci-functions.rst
      - conf.py
    requirements:
      - sphinx-theme

  - name: "project-info"
    uri: "https://gitlab.com/dreamer-labs/piperci/piperci-boilerplates.git"
    version: "master"
    files:
      - README.md
      - name: contributing
        path: CONTRIBUTING.md
      - name: LICENSE
        path: LICENSE
        type: text
        highlight: text
```

See the component and files sections for more detail

### Blend static with auto-gen docs

Both static and docs.yml defined content may be used to build your site.

Simply place any static rst files in `source` as source is first copied into place during each `make stage`

It is recommended however to keep your content separate from this config repo by defining a component included from an external repository.

### Components from a local directory

Component URI's may point to a local directory... for example, point to components in this repository each under their own sub-directory tree.

```
  - name: "my_component"
    uri: "components/my_component"
    version: "master"
    files:
      - README.md
      - name: contributing
        path: CONTRIBUTING.md
      - name: LICENSE
        path: LICENSE
        type: text
        highlight: text
```

### Docs.yml syntax

#### Component Directives

```yaml
name: :string: The name of the target project directory for example 'python-piperci' which would create the component at /python-piperci
uri: :string: The git clone URI or local file path to a component(sub-directory). It must be a git URI with access by the build user.
version: :string: The tag, branch or hash to checkout
files: :list: An optional list of "files" definitions. Optional if you want more then the README.md/rst auto-include behavior. Files may be a list of strings or dictionaries with further options. See: files options.
site_root: :boolean: A flag to indicate that the content here should be saved to the site root. This is used to indicate that the named component is copied to the site root. Example: /index.rst rather then /my_compontent/index.rst etc.
requirements: :list: A list of python requirements for this component
```

Supported "file" level directives are defined below. Options are separated by type starting with global shared file directives.

#### Shared File Directives

```yaml
name:
  - ":string: The name of the file entry as it will live in the docs"
  - ":required:"
path:
  - ":string: The relative path to the file in the source project."
  - ":default: name"
type:
  - ":string: A documentation file type."
  - ":choices: ['md', 'rst', 'autodoc', 'text', 'index']"
  - ":default: [md -> rst] in order."
add_toc:
  - ":boolean: If true will attempt to add/append a sub-section table of contents to the and of this file."
  - ":default: false"
```

#### Autodoc File Directives

```yaml
type: :string: "autodoc"
title: :string: A title to be used for this autogenerated page
description: :string: A freeform text blob that will be displayed below the "title".
programs:
  - ":string: A program definition (text/code block with program usage/help etc.)"
  - ":list: A list of program definiation text blocks"
imports:
  - ":string: A string with one import directive... for this entire page"
  - ":list: A list of import directives showing users how to import these tools"
  - ":example: from x.y import z"
modules:
  - ":list: A list of module definitions"
  - ":required:"
  - ":example: See below of"
  - heading: :string: Text for this modules "heading" h2
    module:
      - ":string: The python module autodoc should import... x.y"
      - ":example: piperci.sri"
    imports:
      - ":string: A string with one import directive... specific for this module"
      - ":list: A list of import directives showing users how to import these tools"
      - ":example: from x.y import z"
requirements:
  - ":list: A list of pip -r requirements.txt entries"
```

#### Index File Directives

```yaml
description: ":string: Section description"
add_toc: ":default: true Normally add_toc is false, but for the type: index it defaults to true"
type: ":string: index"
```

#### Text File Directives

```yaml
description: ":string: Description"
highlight: ":string: pygments language lexer http://pygments.org/docs/lexers/ shortnames"
```

#### Openapi Directives

```yaml
description: ":string: Description"
```
