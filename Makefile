SHELL := /bin/bash

include make.conf

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -b help "$(BUILDDIR)" "$(DESTDIR)" $(SPHINXOPTS) $(O)

clean:
	@[[ -d "artifact/" ]] && rm -rf artifact/ && echo 'removed artifact' || echo 'artifact already clean'

clean-cache:
	@[[ -d "$(CACHEDIR)" ]] && rm -rf $(CACHEDIR) && echo "removed $(CACHEDIR)" || echo 'cache already clean'

clean-build:
	@[[ -d "$(BUILDDIR)" ]] && rm -rf $(BUILDDIR) && echo "removed $(BUILDDIR)" || echo 'builddir already clean'

clean-all: clean clean-cache clean-build
	@echo 'Cleaned all targets'

$(BUILDDIR):
	@cp -r $(SOURCEDIR) $(BUILDDIR)

$(CACHEDIR):
	@mkdir $(CACHEDIR)

$(CACHEDIR)/components: $(BUILDDIR)
	@mkdir $(CACHEDIR)/components

stage: $(CACHEDIR) $(CACHEDIR)/components $(BUILDDIR)
	@python3 scripts/stage_docs.py --release $(RELEASE) --cache-dir $(CACHEDIR)/components --build-dir $(BUILDDIR) --component-requirements $$( [[ -n "$(DOCS_YML)" ]] && echo "--docs-yaml-uri $(DOCS_YML)"; [[ -n "$(RELEASE_YML)" ]] && echo "--release-file-uri $(RELEASE_YML)" )

stage-cached: $(CACHEDIR) $(BUILDDIR)
	@python3 scripts/stage_docs.py --release $(RELEASE) --cache-dir $(CACHEDIR)/components --build-dir $(BUILDDIR) --component-requirements $$( [[ -n "$(DOCS_YML)" ]] && echo "--docs-yaml-uri $(DOCS_YML)"; [[ -n "$(RELEASE_YML)" ]] && echo "--release-file-uri $(RELEASE_YML)" ) --use-cached

update-local:
	rsync -ruv $(SOURCEDIR)/* $(BUILDDIR)

requirements:
	scripts/system_requirements.sh
	pip3 install -r requirements.txt

component-requirements:
	pip3 install -r $(BUILDDIR)/component-requirements.txt

html: Makefile
	@$(SPHINXBUILD) -d "$(CACHEDIR)" -E -a -b html "$(BUILDDIR)" "$(DESTDIR)"

markdown: Makefile
	@$(SPHINXBUILD) -d "$(CACHEDIR)" -E -a -b markdown "$(BUILDDIR)" "$(DESTDIR)"

spellcheck: update-local
	@scripts/spell-check.sh

linkcheck: update-local html
	@scripts/link-check.sh

.PHONY: stage stage-cached help Makefile clean cleanall deps requirements component-requirements update-local spellcheck linkcheck component-requirements
